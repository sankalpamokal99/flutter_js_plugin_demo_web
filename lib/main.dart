import 'package:flutter/material.dart';
import 'package:flutter_js_plugin_demo_web/qrscan.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'QR Scanner JS Plugin Demo',
      home: QrScanDemo(),
    );
  }
}
