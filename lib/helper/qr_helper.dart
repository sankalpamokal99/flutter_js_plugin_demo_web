// ignore: avoid_web_libraries_in_flutter
import 'dart:html';
// ignore: avoid_web_libraries_in_flutter
import 'dart:js' as js;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class QRHelper {
  // QR widget to insert into the tree
  static Widget QRWidget;
  //html video element
  static VideoElement qrVideoElement;

  @override
  static void init() {
    qrVideoElement = VideoElement();
    ui.platformViewRegistry
        .registerViewFactory('qr-reader', (int viewId) => qrVideoElement);

    QRWidget = HtmlElementView(key: UniqueKey(), viewType: 'qr-reader');

    var session = {"audio": false, "video": true};

    window.navigator.mediaDevices
        .getUserMedia(session)
        .then((MediaStream stream) {
      qrVideoElement.srcObject = stream;
    });

    //calling javascript method
    js.context.callMethod('qrCode', [qrVideoElement]);
  }
}
