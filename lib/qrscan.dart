import 'dart:html' as html;
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_js_plugin_demo_web/helper/qr_helper.dart';

class QrScanDemo extends StatefulWidget {
  @override
  _QrScanDemoState createState() => _QrScanDemoState();
}

class _QrScanDemoState extends State<QrScanDemo> {
  String qrString;

  @override
  void initState() {
    super.initState();
    QRHelper.init();
    html.window.addEventListener("qrStringFound", (event) {
      CustomEvent qrEvent = event;
      qrString = qrEvent.detail;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("QR Scanner JS Plugin Demo"),
      ),
      backgroundColor: Color(0xffEFF4F8),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 320,
              height: 270,
              child: Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                child: Container(
                  height: 220,
                  width: 220,
                  child: QRHelper.QRWidget,
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  color: Colors.white),
            ),
            SizedBox(
              height: 20,
            ),
            qrString != null
                ? Container(height: 30,
                  child: Text(
                      "QR Data: $qrString",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20, decoration: TextDecoration.none),
                    ),
                )
                : SizedBox(height: 30,)
          ],
        ),
      ),
    );
  }
}
