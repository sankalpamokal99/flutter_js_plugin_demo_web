function qrCode(video) {
    var qrString;
    // creating scanner object
    let scanner = new Instascan.Scanner({
        video: video
    });

    // checking for available cameras
    Instascan.Camera.getCameras().then(function(cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function(e) {
        console.error(e);
    });

    //    listening if qr found
    scanner.addListener('scan', function(qrString) {
        window.dispatchEvent(new CustomEvent('qrStringFound', {
                    "detail": qrString
            }));
        });
}
